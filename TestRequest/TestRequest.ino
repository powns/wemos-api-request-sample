#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

int requestIndex = 0;
 
const char* ssid = "ssid";
const char* password = "ww";

void setup () {
 
  Serial.begin(9600);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
 
    delay(1000);
    Serial.print("Connecting..");
 
  }
 
}
 
void loop() {
  //getMethod();
  
  //Werkt nog niet helemaal, nog geen idee hoe je een post correct aanroept...
  postMethod();
}

void postMethod(){
  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
   
   http.begin("http://jsonplaceholder.typicode.com/users");      //Specify request destination
   http.addHeader("Content-Type", "text/plain");  //Specify content-type header
 
   int httpCode = http.POST("test");   //Send the request
   String payload = http.getString();                  //Get the response payload
 
   Serial.println(httpCode);   //Print HTTP return code
   Serial.println(payload);    //Print request response payload
 
   http.end();  //Close connection
 
 }

 delay(10000);
 
}

void getMethod() {
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
    requestIndex++;
    
    if (requestIndex > 4){
      requestIndex = 1;
    }

    Serial.println(requestIndex);
    
    HTTPClient http;  //Declare an object of class HTTPClient

    String link = "http://jsonplaceholder.typicode.com/users/";
    String link2 = link + requestIndex;

    Serial.println(link2);
    
    http.begin(link2);  //Specify request destination
    int httpCode = http.GET();                                                                  //Send the request
 
    if (httpCode > 0) { //Check the returning code
 
      String payload = http.getString();   //Get the request response payload
      Serial.println(payload);                     //Print the response payload
 
    }
 
    http.end();   //Close connection
 
  }

  Serial.println("setting delay");
  delay(5000);    //Send a request every 30 seconds
}
